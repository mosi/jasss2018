# Readme

This repository contains all models and SESSL-experiments from the paper: 

Reinhardt, O., Hilton, J. D., Warnke, T., Bijak, J., Uhrmacher, A. M. (2018). "Streamlining simulation experiments with agent-based models in demography" In: Journal of Artificial Societies and Social Simulation (to appear).

The content of the repository can be downloaded [here](https://git.informatik.uni-rostock.de/mosi/jasss2018/repository/master/archive.zip).

#### Structure of the repository
* The repository contains 3 directories:
    * *healthcare*: The social care model and experiment in figure 14
    * *migration*: The return migration models and the experiments in figures 9 and 11
    * *prey-predator*: The prey-predator model in ML-Rules and ML3, and the experiments in figures 1 and 4

#### System requirements
* Java Runtime Environment (JRE) 8 (https://www.java.com)

#### Further information

* For more information about ML3, see the [ML3 repository](https://git.informatik.uni-rostock.de/mosi/ml3)
* For more information about SESSL, see [sessl.org](http://sessl.org)

#### Start the experiments
* Download the content of the repository [here](https://git.informatik.uni-rostock.de/mosi/jasss2018/repository/master/archive.zip)
* For the statistical model checking experiments in figures 1 and 4:
    * Navigate to the directory *prey-predator*
    * The experiment code shown in the figures can be found in the file *Example.scala*
    * In the file *pom.xml* it has to be configured, which of the experiments shall be executed:
        * To execute the experiment with the ML-Rules model (figure 1), make sure line 51 says: 
        
            ```
            <mainClass>org.sessl.MLRulesExperiment</mainClass>
            ```
            
        * To execute the experiment with the ML3 model (figure 4), make sure line 51 says: 
        
            ```
            <mainClass>org.sessl.ML3Experiment</mainClass>
            ```
            
    * To execute the experiment:
        * Windows: execute *run.bat*
        * Linux: execute *run.sh*
    * Both experiments should execute in less than a minute
* For the experiments with the return migration model in figures 9 and 11:
    * Navigate to the directory *migration*
    * The experiment code shown the the figures can be found in the files *CCExperiment.scala* (figure 9) and *LHCExperiment.scala* (fiure 11)
    * In the file *pom.xml* it has to be configured, which of the experiments shall be executed:
        * To execute the experiment from figure 9, make sure line 51 says:
        
            ```
            <mainClass>org.sessl.CCExperiment</mainClass>
            ```
           
        * To execute the experiment from figure 11, make sure line 51 says:
        
            ```
            <mainClass>org.sessl.LHCExperiment</mainClass>
            ```
           
    * To execute the experiment:
        * Windows: execute *run.bat*
        * Linux: execute *run.sh*
    * Both experiments should execute in less than 30 minutes
* For the experiments with the return model of social care in figure 14:
    * Navigate to the directory *healthcare*
    * The experiment code shown in figure 14 can be found in the directory *src/main/scala/sessl/healthcare* in the file *Experiment.scala*
    * To execute the experiment:
        * Windows: execute *run.bat*
        * Linux: execute *run.sh*
    * The experiment will take multiple hours (e.g., 12.5 hours on the powerful desktop machine used for measuring runtime for the paper)

* The Maven wrapper (https://github.com/takari/maven-wrapper) will automatically download and invoke Apache Maven (https://maven.apache.org), after the experiments are stated
* Maven is automatically downloading all needed dependencies and additional software, before the simulation begins

#### Result format
* Statistical model checking experiments:
    * The experiments will print *true* on the terminal, to indicate that the specified property is fulfilled
* Experiments with the return migration model:
    * The experiment for the linear regression metamodel (*CCExperiment.scala*):
        * The fitted metamodel is printed to the terminal, followed by a list of standardized regression coefficients, and R^2
        * In addition, the experiment creates a folder *results-YYYYMMDD-HHMMSS*, where *YYYYMMDD-HHMMSS* is a timestamp that is differen each time the experiment is executed
        * The folder contains the number of migrants, observed each year, for all simulation runs
        * Each of the folders *config-N* contains the data for all replications of a single configurations
        * The configuration is specified in *config.csv*, the data for each run can be found in a file *run-N.csv*
    * The experiment for the gaussian process metamodel (*LHCExperiment.scala*):
        * The experiments produces a result folder in the same format as *CCExperiment.scala*
* Experiment with the model of social care:
    * The experiment will print the optimal parameter value and the value of the optimum to the terminal, e.g.:

        ```
        Overall results: (Map(ageOfRetirement -> 73.26947004324244),objective =74.94546979509414 [min])
        ```