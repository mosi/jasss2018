package org.jamesii.ml3.healthcare;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.jamesii.core.util.misc.Pair;
import org.jamesii.ml3.model.agents.IAgent;
import org.jamesii.ml3.model.state.IState;
import org.jamesii.ml3.model.values.IntValue;
import org.jamesii.ml3.model.values.StringValue;
import org.jamesii.ml3.observation.IListener;
import org.jamesii.ml3.simulator.simulators.RuleInstance;

/**
 * Listener that reports the total weekly healthcare cost in the current state.
 * 
 * @author Oliver Reinhardt
 *
 */
public class HealthcareListener implements IListener {
	/**
	 * Healthcare cost per hour.
	 */
	public static final double COST_PER_HOUR = 20;
	/**
	 * Care demand by care need level.
	 */
	public static final double[] CARE_DEMAND_BY_LEVEL = { 0, 8, 16, 30, 80 };
	/**
	 * Available care supply by status (child, adult at home, independent adult,
	 * retired).
	 */
	private static final double[] CARE_SUPPLY_BY_STATUS = { 5, 30, 25, 60 };

	/**
	 * Callback method.
	 */
	private Consumer<Pair<Double, CareDemandReport>> callback;

	public HealthcareListener(Consumer<Pair<Double, CareDemandReport>> callback) {
		this.callback = callback;
	}

	@Override
	public void notify(IState state, double time, RuleInstance instance, IAgent agent) {
		CareDemandReport report = new CareDemandReport();
		Map<IAgent, Double> careSupplies = new HashMap<>();
		Map<IAgent, Double> careDemands = new HashMap<>();
		
		report.time = time;
		report.malesByAge = new double[30];
		report.femalesByAge = new double[30];
		
		// get all persons
		Collection<IAgent> population = state.getAgentsAliveByType("Person");
		report.totalPopulation = population.size();

		// set care demand a supply for all persons
		for (IAgent person : population) {
			int careDemandLevel = ((IntValue) person.getAttributeValue("careNeedLevel")).getValue();
			String status = ((StringValue) person.getAttributeValue("status")).getValue();
			String sex = ((StringValue) person.getAttributeValue("sex")).getValue();

			if (status.equals("adult at home") || status.equals("independent adult")) {
				report.taxpayerPopulation++;
			}
			
			if (sex.equals("f")) {
				report.femalesByAge[(int) (person.getAge(time)/5)]++;
			} else {
				report.malesByAge[(int) (person.getAge(time)/5)]++;
			}
			
			// care demand by care need level from the paper
			if (careDemandLevel > 0) {
				careDemands.put(person, CARE_DEMAND_BY_LEVEL[careDemandLevel]);
				report.totalCareHours += CARE_DEMAND_BY_LEVEL[careDemandLevel];
			}

			// full care supply for everyone without care need, half care supply
			// for everyone with care need level 1, no care supply for everybody
			// else
			if (careDemandLevel == 0) {
				double careSupply = getCareSupplyByStatus(status);
				careSupplies.put(person, careSupply);
				report.maximumInformalCareHours += careSupply;
			} else if (careDemandLevel == 1) {
				double careSupply = getCareSupplyByStatus(status) / 2;
				careSupplies.put(person, careSupply);
				report.maximumInformalCareHours += careSupply;
			}
		}

		// assign carers to carees to determine the amount of informal care
		for (IAgent person : careDemands.keySet()) {
			List<IAgent> carers = new ArrayList<>();
			IAgent town = person.getLinkedAgents("house").iterator().next().getLinkedAgents("town").iterator().next();

			// add occupants of the same house, without self
			person.getLinkedAgents("house").iterator().next().getLinkedAgents("occupants").stream()
					.forEach(occupant -> {
						if (!occupant.equals(person))
							carers.add(occupant);
					});
			// add children from all marriages
			person.getLinkedAgents("couples").forEach(couple -> {
				Collection<IAgent> children = couple.getLinkedAgents("children").stream().filter(a -> a.isAlive()).collect(Collectors.toSet());
				for (IAgent child : children) {
					if (child.getLinkedAgents("house").iterator().next().getLinkedAgents("town").iterator().next().equals(town)) {
						carers.add(child);
					}
				}
			});

			// assign care supply from possible carers
			double careDemand = careDemands.get(person);
			Iterator<IAgent> iterator = carers.iterator();
			while (iterator.hasNext() && careDemand > 0) {
				IAgent carer = iterator.next();
				if (careSupplies.containsKey(carer)) {
					double careSupply = careSupplies.get(carer);
					double careHours = Math.min(careSupply, careDemand);
					careDemand -= careHours;
					double remainingCareSupply = careSupply - careHours;
					if (remainingCareSupply > 0) {
						careSupplies.put(carer, remainingCareSupply);
					} else {
						careSupplies.remove(carer);
					}
					report.informalCareHours += careHours;
				}
			}
		}

		report.formalCareHours = report.totalCareHours - report.informalCareHours;
		report.formalCareCost = report.formalCareHours * COST_PER_HOUR;
		report.formalCareCostPerTaxpayer = report.formalCareCost / report.taxpayerPopulation;
		
		int hhPersons = 0;
		for (IAgent house : state.getAgentsAliveByType("House")) {
			int size = house.getLinkedAgents("occupants").stream().filter(a -> a.isAlive()).collect(Collectors.toList()).size();
			if (size > 0) {
				report.households++;
				hhPersons += size;
			}
		}
		
		report.averageHouseholdSize = ((double) hhPersons) / report.households;
		
		if (hhPersons != report.totalPopulation) {System.out.println("something's wrong");}

		callback.accept(new Pair<>(time, report));
	}

	private double getCareSupplyByStatus(String status) {
		switch (status) {
		case "child":
			return CARE_SUPPLY_BY_STATUS[0];
		case "adult at home":
			return CARE_SUPPLY_BY_STATUS[1];
		case "independent adult":
			return CARE_SUPPLY_BY_STATUS[2];
		case "retired":
			return CARE_SUPPLY_BY_STATUS[3];
		default:
			return 0;
		}
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void finish() {
		// do nothing
	}

	public static class CareDemandReport {
		private int totalPopulation;
		private int taxpayerPopulation;
		private double totalCareHours;
		private double maximumInformalCareHours;
		private double informalCareHours;
		private double formalCareHours;
		private double formalCareCost;
		private double formalCareCostPerTaxpayer;
		private double time;
		private double[] malesByAge;
		private double[] femalesByAge;
		private int households;
		private double averageHouseholdSize;
		
		public double getTime() {
			return time;
		}

		public int getTotalPopulation() {
			return totalPopulation;
		}

		public int getTaxpayerPopulation() {
			return taxpayerPopulation;
		}

		public double getTotalCareHours() {
			return totalCareHours;
		}

		public double getInformalCareHours() {
			return informalCareHours;
		}

		public double getFormalCareHours() {
			return formalCareHours;
		}

		public double getMaximumInformalCareHours() {
			return maximumInformalCareHours;
		}

		public double getFormalCareCost() {
			return formalCareCost;
		}

		public double getFormalCareCostPerTaxpayer() {
			return formalCareCostPerTaxpayer;
		}

		public double[] getMalesByAge() {
			return malesByAge;
		}
		
		public double[] getFemalesByAge() {
			return femalesByAge;
		}

		public int getHouseholds() {
			return households;
		}

		public double getAverageHouseholdSize() {
			return averageHouseholdSize;
		}

	}

}
