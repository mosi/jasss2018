package org.jamesii.ml3.healthcare;

import org.apache.commons.math3.random.RandomGenerator;
import org.jamesii.ml3.experiment.init.IInitialStateBuilder;
import org.jamesii.ml3.model.Model;
import org.jamesii.ml3.model.Parameters;
import org.jamesii.ml3.model.agents.IAgent;
import org.jamesii.ml3.model.agents.IAgentFactory;
import org.jamesii.ml3.model.state.IState;
import org.jamesii.ml3.model.state.IStateFactory;
import org.jamesii.ml3.model.values.IntValue;
import org.jamesii.ml3.model.values.StringValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Initial state builder for the healthcare model.
 *
 * @author Oliver Reinhardt
 */
public class HealthcareStateBuilder implements IInitialStateBuilder {
	/* House and Town paramters from the python model. */
	private int mapGridXDimension = 8;
	private int mapGridYDimension = 12;
	private int townGridDimension = 25;
	private int numOfHouseClasses = 3;
	private double[] cdfHouseClasses = {0.6, 0.9, 5.0};
	private double[][] ukMap = {{0.0, 0.1, 0.2, 0.1, 0.0, 0.0, 0.0, 0.0}, {0.1, 0.1, 0.2, 0.2, 0.3, 0.0, 0.0, 0.0},
			{0.0, 0.2, 0.2, 0.3, 0.0, 0.0, 0.0, 0.0}, {0.0, 0.2, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0},
			{0.4, 0.0, 0.2, 0.2, 0.4, 0.0, 0.0, 0.0}, {0.6, 0.0, 0.0, 0.3, 0.8, 0.2, 0.0, 0.0},
			{0.0, 0.0, 0.0, 0.6, 0.8, 0.4, 0.0, 0.0}, {0.0, 0.0, 0.2, 1.0, 0.8, 0.6, 0.1, 0.0},
			{0.0, 0.0, 0.1, 0.2, 1.0, 0.6, 0.3, 0.4}, {0.0, 0.0, 0.5, 0.7, 0.5, 1.0, 1.0, 0.0},
			{0.0, 0.0, 0.2, 0.4, 0.6, 1.0, 1.0, 0.0}, {0.0, 0.2, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0}};
	private double[][] ukClassBias = {{0.0, -0.05, -0.05, -0.05, 0.0, 0.0, 0.0, 0.0},
			{-0.05, -0.05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}, {0.0, -0.05, -0.05, 0.0, 0.0, 0.0, 0.0, 0.0},
			{0.0, -0.05, -0.05, 0.05, 0.0, 0.0, 0.0, 0.0}, {-0.05, 0.0, -0.05, -0.05, 0.0, 0.0, 0.0, 0.0},
			{-0.05, 0.0, 0.0, -0.05, -0.05, -0.05, 0.0, 0.0}, {0.0, 0.0, 0.0, -0.05, -0.05, -0.05, 0.0, 0.0},
			{0.0, 0.0, -0.05, -0.05, 0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, -0.05, 0.0, -0.05, 0.0, 0.0, 0.0},
			{0.0, 0.0, 0.0, -0.05, 0.0, 0.2, 0.15, 0.0}, {0.0, 0.0, 0.0, 0.0, 0.1, 0.2, 0.15, 0.0},
			{0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0}};
	private double mapDensityModifier = 0.5;

	/* Population parameters from the python model. */
	private int initialPop = 750;
	private int startYear = 1860;
	private int minStartAge = 20;
	private int maxStartAge = 40;

	private Model model;
	private IAgentFactory agentFactory;
	private RandomGenerator rng;

	private IState state;

	@Override
	public synchronized IState buildInitialState(Model model, IStateFactory stateFactory, IAgentFactory agentFactory,
												 RandomGenerator rng, Parameters parameters) {
		this.model = model;
		this.agentFactory = agentFactory;
		this.rng = rng;

		state = stateFactory.create();

		buildTownGrid();

		List<IAgent> couples = createPopulation();

		distribute(couples);

		state.addAgent(agentFactory.createAgent(model.getAgentDeclaration("MarriageMarket"), 0));

		return state;
	}

	/**
	 * Distributes the couples to random houses and sets their sec.
	 *
	 * @param couples
	 */
	private void distribute(List<IAgent> couples) {
		List<IAgent> houses = new ArrayList<>(state.getAgentsByType("House"));
		for (IAgent couple : couples) {
			int r = rng.nextInt(houses.size());
			IAgent house = houses.get(r);
			houses.remove(r);
			for (IAgent spouse : couple.getLinkedAgents("spouses")) {
				spouse.setAttributeValue("sec", house.getAttributeValue("sec"));
				spouse.addLink("house", house);
				house.addLink("occupants", spouse);
			}
		}
	}

	/**
	 * Creates a
	 *
	 * @return list of the couples
	 */
	private List<IAgent> createPopulation() {
		List<IAgent> couples = new ArrayList<>();
		for (int i = 0; i < initialPop / 2; i++) {
			couples.add(createCouple());
		}
		return couples;
	}

	/**
	 * Creates a couple and dead parents for both persons.
	 *
	 * @return a couple agent
	 */
	private IAgent createCouple() {
		double startAge = minStartAge + rng.nextDouble() * (maxStartAge - minStartAge);
		double timeOfBirth = startYear - startAge;

		// create male
		IAgent m = agentFactory.createAgent(model.getAgentDeclaration("Person"), timeOfBirth);
		m.setAttributeValue("sex", new StringValue("m"));
		m.setAttributeValue("status", new StringValue("independent adult"));

		// create male's parents
		IAgent mParents = agentFactory.createAgent(model.getAgentDeclaration("Couple"), 0);
		mParents.die(0);
		IAgent mMother = agentFactory.createAgent(model.getAgentDeclaration("Person"), 0);
		mMother.setAttributeValue("sex", new StringValue("f"));
		mMother.setAttributeValue("status", new StringValue("retired"));
		mMother.die(0);
		mMother.addLink("couples", mParents);
		mParents.addLink("spouses", mMother);
		IAgent mFather = agentFactory.createAgent(model.getAgentDeclaration("Person"), 0);
		mFather.setAttributeValue("sex", new StringValue("m"));
		mFather.setAttributeValue("status", new StringValue("retired"));
		mFather.die(0);
		mFather.addLink("couples", mParents);
		mParents.addLink("spouses", mFather);

		m.addLink("parents", mParents);
		mParents.addLink("children", m);

		// create female
		IAgent f = agentFactory.createAgent(model.getAgentDeclaration("Person"), timeOfBirth);
		f.setAttributeValue("sex", new StringValue("f"));
		f.setAttributeValue("status", new StringValue("independent adult"));

		// create female's parents
		IAgent fParents = agentFactory.createAgent(model.getAgentDeclaration("Couple"), 0);
		fParents.die(0);
		IAgent fMother = agentFactory.createAgent(model.getAgentDeclaration("Person"), 0);
		fMother.setAttributeValue("sex", new StringValue("f"));
		fMother.setAttributeValue("status", new StringValue("retired"));
		fMother.die(0);
		fMother.addLink("couples", fParents);
		fParents.addLink("spouses", fMother);
		IAgent fFather = agentFactory.createAgent(model.getAgentDeclaration("Person"), 0);
		fFather.setAttributeValue("sex", new StringValue("m"));
		fFather.setAttributeValue("status", new StringValue("retired"));
		fFather.die(0);
		fFather.addLink("couples", fParents);
		fParents.addLink("spouses", fFather);

		f.addLink("parents", fParents);
		fParents.addLink("children", f);

		// creates the couple
		IAgent couple = agentFactory.createAgent(model.getAgentDeclaration("Couple"), startYear);
		m.addLink("couples", couple);
		couple.addLink("spouses", m);
		f.addLink("couples", couple);
		couple.addLink("spouses", f);

		// add all agents to the state
		state.addAgent(m);
		state.addAgent(mParents);
		state.addAgent(mMother);
		state.addAgent(mFather);
		state.addAgent(f);
		state.addAgent(fParents);
		state.addAgent(fMother);
		state.addAgent(fFather);
		state.addAgent(couple);

		return couple;
	}

	/**
	 * Make a town for each grid cell.
	 */
	private void buildTownGrid() {
		for (int y = 0; y < mapGridYDimension; y++) {
			for (int x = 0; x < mapGridXDimension; x++) {
				buildTown(x, y, ukMap[y][x], ukClassBias[y][x]);
			}
		}
	}

	/**
	 * Creates a town and the houses.
	 *
	 * @param x         town grid x
	 * @param y         town grid y
	 * @param density   town denisty
	 * @param classBias town class bias
	 */
	private void buildTown(int x, int y, double density, double classBias) {
		IAgent newTown = agentFactory.createAgent(model.getAgentDeclaration("Town"), 0);
		newTown.setAttributeValue("x", new IntValue(x));
		newTown.setAttributeValue("y", new IntValue(y));

		if (density > 0) {
			double adjustedDensity = density * mapDensityModifier;
			for (int hy = 0; hy < townGridDimension; hy++) {
				for (int hx = 0; hx < townGridDimension; hx++) {
					if (rng.nextDouble() < adjustedDensity) {
						buildHouse(newTown, classBias);
					}
				}
			}
		}

		state.addAgent(newTown);
	}

	/**
	 * Create a house and link it to the town.
	 *
	 * @param town      the town
	 * @param classBias the town class bias
	 */
	private void buildHouse(IAgent town, double classBias) {
		IAgent newHouse = agentFactory.createAgent(model.getAgentDeclaration("House"), 0);
		newHouse.addLink("town", town);
		town.addLink("houses", newHouse);
		double r = rng.nextDouble();
		int i = 0;
		double c = cdfHouseClasses[i] - classBias;
		while (r > c) {
			i++;
			c = (i < numOfHouseClasses) ? cdfHouseClasses[i] - classBias : 1;
		}
		newHouse.setAttributeValue("sec", new IntValue(i));
		state.addAgent(newHouse);
	}
}
