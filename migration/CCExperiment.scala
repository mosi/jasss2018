package org.sessl

import sessl._
import sessl.ml3._

object CCExperiment extends App {
  execute {

    new Experiment with Observation with ParallelExecution with ParameterMaps with CSVOutput with CentralCompositeDesign with LinearRegression {
      model = "migration.ml3"
      simulator = NextReactionMethod()
      parallelThreads = -1

      initializeWith(Expressions("new Universe()"))
      startTime = 0
      stopTime = 500

      // load from file below - contains cumulative probability of being age x and below.
      fromFile("ageAtMigration.csv")("ageAtMigration")

      set("mu" <~ 100) // mean income target
      set("sigma" <~ 10) // variance in savings target
      set("pi0" <~ 3) // 3 jobs / year.
      set("pi1" <~ 0.00) // zero for the moment.
      set("alpha" <~ 1.5) // wage parameters
      set("beta" <~ 0.0001) // wage parameters
      set("c_h" <~ 1.0) // minimum earnings
      set("z" <~ 0.5) // 1/2 year wait before returning

      //  Define intervals for varied parameters, and define CCD experiment
      centralComposite("theta" <~ interval(0.0, 1.0), "gamma" <~ interval(0.0, 2.0), "tau" <~ interval(0.0, 0.5), "m" <~ interval(2.0, 6.0))
      replications = 10

      // baseline inflow
      set("constantInflowRate" <~ 52)
      // 1 per week

      observeAt(range(0, 1, 500))
      val migrants = observe("migrants" ~ agentCount("Migrant"))

      withExperimentResult { result =>
        // fit a linear regression model with all quadratic terms
        val regr = fitLinearModel(result)("theta", "gamma", "tau", "m", "theta" * "theta", "theta" * "gamma", "theta" * "tau", "gamma" * "gamma", "gamma" * "tau", "tau" * "tau", "m" * "gamma", "m" * "theta", "m" * "tau", "m" * "m")(migrants)
        println(regr.fittedFunction)
        println(regr.standardizedBeta mkString " ")
        println(regr.rSquared)
        writeCSV(result)
      }
    }
  }
}