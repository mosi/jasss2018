package org.sessl

object MLRulesExperiment extends App {

  import sessl._
  import sessl.mlrules._
  import sessl.verification._
  import sessl.verification.mitl._

  execute {
    new Experiment with Observation with StatisticalModelChecking {
      model = "./prey-predator.mlrj"
      simulator = SimpleSimulator()
      stopTime = 11
      val prey = observe(count("*/Prey")); val predator = observe(count("*/Predator"))
      observeAt(range(0.0, 0.1, 11))

      set("a" <~ 0.014, "b" <~ 0.6, "d" <~ 0.7, "k" <~ 0.002)

      prop = MITL(G(0, 10)((OutVar(prey) > Constant(0)) and (OutVar(predator) > Constant(0))))
      test = SequentialProbabilityRatioTest(p = 0.8, alpha = 0.05, beta = 0.05, delta = 0.05)

      withCheckResult { result =>
        println(result.satisfied)
      }
    }
  }
}

object ML3Experiment extends App {

  import sessl._
  import sessl.ml3._
  import sessl.verification._
  import sessl.verification.mitl._

  execute {
    new Experiment with Observation with StatisticalModelChecking {
      model = "./prey-predator.ml3"
      simulator = NextReactionMethod()
      stopTime = 11
      val prey = observe(agentCount("Prey")); val predator = observe(agentCount("Predator"))
      observeAt(range(0.0, 0.1, 11))

      set("a" <~ 0.014, "b" <~ 0.6, "d" <~ 0.7, "k" <~ 0.002)

      initializeWith(Expressions(100 * "new Prey()", 10 * "new Predator()"))

      prop = MITL(G(0, 10)((OutVar(prey) > Constant(0)) and (OutVar(predator) > Constant(0))))
      test = SequentialProbabilityRatioTest(p = 0.8, alpha = 0.05, beta = 0.05, delta = 0.05)

      withCheckResult { result =>
        println(result.satisfied)
      }
    }
  }
}
